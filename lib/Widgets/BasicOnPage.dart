import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:killing_time_notepatch/Pages/DeleteUser.dart';
import 'package:killing_time_notepatch/Pages/GetUser.dart';

import '../Pages/CreateUser.dart';


class BasicOnPage {

  static int currentIndex = 0;
  static var versions = [];

  static Widget ApBar(Title, color) {
    return AppBar(
      backgroundColor: color,
      title: Text(
        Title,
        style: TextStyle(
            color: Colors.white
        ),
      ),

      actions: <Widget>[
        Builder(
          builder: (context) =>
              GestureDetector(
                onTap: () {
                  exit(0);
                },
                child: Icon(
                  Icons.exit_to_app,
                  color: Colors.white,
                  size: 40,
                ),
              ),
        )
      ],
    );
  }


  static List MenuBot(context) {
    return <BottomNavigationBarItem>[
      MenuButon(Icons.add, 'Create', Colors.black, Colors.green, 2),
      MenuButon(Icons.remove_red_eye_outlined, 'Get', Colors.black, Color(0xFFD1C4E9), 2),
      MenuButon(Icons.remove, 'Delete', Color(0xFF1B5E20), Colors.red, 2),
    ];
  }


  static BottomNavigationBarItem MenuButon(icon, title, color, bk, type) {
    if (type == 1) {
      return BottomNavigationBarItem(
        icon: SvgPicture.asset(
          icon,
          width: 25,
        ),
        label: title,
        backgroundColor: bk,
      );
    }
    else if (type == 2) {
      return BottomNavigationBarItem(
        icon: Icon(
          icon,
          color: color,
        ),
        label: title,
        backgroundColor: bk,
      );
    }
  }

  static void onTabTapped(context, index) {
    switch (index) {
      case 0:
        {
          Navigator.pushNamed(context, CreateUser.routeName);
          //Navigator.push(context, MaterialPageRoute(builder: (context) => MenuBottom()));
        }
        break; // Create this function, it should return your first page as a widget
      case 1:
        {
          Navigator.pushNamed(context, GetUser.routeName);
          //  Navigator.push(context, MaterialPageRoute(builder: (context) => search()));
        }
        break; // Create this function, it should return your second page as a widget
      case 2:
        {
          Navigator.pushNamed(context, DeleteUser.routeName);
          // Navigator.push(context, MaterialPageRoute(builder: (context) => upload()));
        }
        break;

    }
  }
}