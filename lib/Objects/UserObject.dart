import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:killing_time_notepatch/Objects/UserObject.dart';

import '../Pages/GetUser.dart';



class UserObject{

  int id;
  String name;
  String surnames;
  String email;
  DateTime birthday;
  int phone;
  String direction;


  UserObject(
      {
        this.id,
        this.name,
        this.surnames,
        this.email,
        this.birthday,
        this.phone,
        this.direction,
      });

  factory UserObject.fromJson(Map<String, dynamic> json) {
    return UserObject(
        id: json['id'],
        name: json['name'],
        surnames: json['surnames'],
        email: json['email'],
        birthday: json['birthday'] as DateTime,
        phone: json['phone'] as int,
        direction: json['direction']);
  }

  static Future<List<UserObject>> addUser(UserObject user) async {

    var url ='https://localhost:9191/users/save/';
    var body = convertObjectToBody(user);

    await http.post(url,
        body: body
    ).then((http.Response response) {
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.contentLength}");
      print(response.headers);
      print(response.request);

    });

  }

  static Future<List<UserObject>> DeleteUser(String mail) async {

    final response =
    await http.get('https://localhost:9191/users/delete/'+mail);

    if (response.statusCode == 200) {
      var tagObjsJson = jsonDecode(response.body)[""] as List;
      return tagObjsJson
          .map((tagJson) => UserObject.fromJson(tagJson))
          .toList();

    } else {
      throw Exception('Failed to load data');
    }
  }

  static Future<List<UserObject>> getUser(String mail) async {

    final response =
    await http.get('https://localhost:9191/users/'+mail);

    if (response.statusCode == 200) {
      var tagObjsJson = jsonDecode(response.body)[""] as List;
      return tagObjsJson
          .map((tagJson) => UserObject.fromJson(tagJson))
          .toList();

    } else {
      throw Exception('Failed to load data');
    }
  }
  static String convertObjectToBody(UserObject user){
    return jsonEncode(
        {
          'id': user.id,
          'name': user.name,
          'surnames': user.surnames,
          'email': user.email,
          'birthday': user.birthday,
          'phone': user.phone,
          'direction': user.direction
        });
  }

  }
