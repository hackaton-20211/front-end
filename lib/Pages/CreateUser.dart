import 'dart:html';

import 'package:flutter/material.dart';
import 'package:killing_time_notepatch/Widgets/BasicOnPage.dart';

class CreateUser extends StatefulWidget {
  static const routeName = '/CreateUser';

  @override
  State<StatefulWidget> createState() => _CreateUser();
}

class _CreateUser extends State<CreateUser> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BasicOnPage.ApBar("Create User", Colors.green),
      body: Text("hola"),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: BasicOnPage.currentIndex,
        onTap: (value) {
          setState(
                () => BasicOnPage.currentIndex = value,
          );
          BasicOnPage.onTabTapped(context, BasicOnPage.currentIndex);
        },
        selectedItemColor: Colors.black,
        items: BasicOnPage.MenuBot(context),
      ),
    );
  }
}
