import 'package:flutter/material.dart';

import 'Pages/CreateUser.dart';
import 'Pages/DeleteUser.dart';
import 'Pages/GetUser.dart';



void main() {
  runApp(MaterialApp(

      title: 'Hackaton',
      theme: new ThemeData(scaffoldBackgroundColor: const Color(0xFFE1F5FE)),

      initialRoute: '/CreateUser',
      routes: {
        //Pages
        CreateUser.routeName: (context) => CreateUser(),
        DeleteUser.routeName: (context) => DeleteUser(),
        GetUser.routeName: (context) => GetUser(),
      }
  ));


}